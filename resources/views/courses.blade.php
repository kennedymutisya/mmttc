@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="/">Home</a></li>
                <li>/</li>
                <li>Courses</li>
            </ul>
            <h2>Courses</h2>
        </div> <!-- /.opacity -->
    </div> <!-- /.theme-inner-banner -->
    <!--
    =============================================
        Our Courses
    ==============================================
    -->
    <div class="our-courses section-margin-top section-margin-bottom">
        <div class="container">
            <div class="row">

                @foreach(\App\Course::all() as $course)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-course-block">
                            <div class="image"><img src="{{ asset('course.jpg') }}" alt=""></div>
                            <div class="text-box">
                                <h5><i class="flaticon-device"></i><a href="#">{{ $course->course }}</a></h5>
                                <p></p>
                                <ul class="clearfix">
                                    <li><i class="flaticon-book"></i></li>
                                    <li><img src="{{ asset('logo.JPG') }}" alt=""></li>
                                    <li>{{ number_format($course->fees,0) }}</li>
                                </ul>
                                <h6>{{ $course->diploma === 1 ? 'Diploma': 'Certificate' }}</h6>
                                <a href="" class="wow fadeInUp animated theme-line-button" data-wow-delay="0.1s">Join
                                    Now</a>
                            </div> <!-- /.text-box -->
                        </div> <!-- /.single-course-block -->
                    </div> <!-- /.col- -->
                @endforeach
            </div> <!-- /.row -->

        </div> <!-- /.container -->
    </div> <!-- /.our-courses -->

@endsection