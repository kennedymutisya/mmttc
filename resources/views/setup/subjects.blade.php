@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-4">
                <subject :subjects="{{\App\Subject::all()}}"></subject>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection