@component('mail::message')
# Hi, {{$applications->fullname}}

We have received your application.

@component('mail::table')
    | Name:       | {{ $applications->fullname }}         |
    | :-------------: |:-------------:|
    |    Address:   |      {{ $applications->Address }} |
    |    Phone:   |      {{ $applications->phone }} |
    |    Email:   |      {{ $applications->email }} |
    |    National ID:   |      {{ $applications->nationalID }} |
    |    D.O.B:   |      {{ $applications->dob }} |
    |    Sex:   |      {{ $applications->sex }} |
@endcomponent

@component('mail::button', ['url' => \Illuminate\Support\Facades\URL::signedRoute('status',$applications->id)])
Track Application
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
