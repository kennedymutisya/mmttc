@extends('layouts.app')

@section('content')

    <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <showapplications :programme='@json(\App\Applications::with('course')->get())'></showapplications>
                </div>
            </div>
        </div>
    </div>

    <!-- End Dashboard -->
@endsection