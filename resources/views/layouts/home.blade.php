<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themazine.com/html/remakri/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 Mar 2019 07:23:41 GMT -->
<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>MMTTC - Kenya</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="{{ asset('front/images/fav-icon/icon.png') }}">


    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/style.css') }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/responsive.css') }}">


    <!-- Fix Internet Explorer -->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54081021-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-54081021-6');
    </script>

</head>

<body>
<div class="main-page-wrapper">

    <!-- ===================================================
        Loading Transition
    ==================================================== -->
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>


    <!--
    =============================================
        Theme Header
    ==============================================
    -->
    <header class="header-wrapper">
        <div class="top-header bg-one">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-xs-12 left-list">
                        <ul>
                            <li><a href="#">Register</a></li>
                            <li><a href="#">Login</a></li>
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 0722 383 285</a></li>
                        </ul>
                    </div> <!-- /.left-list -->
                    <div class="col-sm-7 col-xs-12 right-list text-right">
                        <ul>
                            {{--                            <li><a href="#">Get Support</a></li>--}}
                            {{--                            <li><a href="#">Our Causes</a></li>--}}
                            {{--                            <li><a href="#">Career</a></li>--}}
                            <li>
                                <ul class="social-icon">
                                    <li><a href="#" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> <!-- /.right-list -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.top-header -->

        <div class="theme-menu-wrapper">
            <div class="container">
                <div class="main-content-wrapper clearfix">
                    <!-- Logo -->
                    <div class="logo float-left" style="padding: 0 !important;margin: 0 !important;">
                        <a href="#">
                            <img src="{{ asset('logo.JPG') }}" height="65" alt="Logo"
                                 style="padding: 0 !important;margin: 0 !important;">
                        </a>
                    </div>

                    <!-- ============================ Theme Menu ========================= -->
                    <nav class="theme-main-menu navbar float-right" id="mega-menu-wrapper">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav">
                                <li class="{{ Request::segment(1) === null ? 'active': '' }}"><a href="/">Home</a></li>
                                <li class="dropdown-holder {{ Request::segment(1) === 'about' ? 'active': '' }}">
                                    <a href="{{ url('about') }}">About Us</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('about') }}">About Us</a></li>
                                        <li><a href="{{ url('about/alumni') }}">Our Alumni</a></li>
                                        <li><a href="{{ url('about/directors-message') }}">Message from Director</a>
                                        </li>
                                        <li><a href="{{ url('about/principal-message') }}">Message from Principal</a>
                                        </li>
                                        <li><a href="{{ url('about/student-leaders') }}">Student Leaders</a></li>
                                        <li><a href="{{ url('gallery') }}">Our Gallery</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder {{ Request::segment(1) === 'schools' ? 'active': '' }}">
                                    <a href="{{ url('schools/medical') }}">Schools</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('schools/medical') }}">Medicine</a></li>
                                        <li><a href="{{ url('schools/engineering') }}">Engineering</a></li>
                                        <li><a href="{{ url('schools/hospitality') }}">Hospitality</a></li>
                                        <li><a href="{{ url('schools/business') }}">Business</a></li>
                                        <li><a href="{{ url('schools/computer-science') }}">Computer Science</a></li>
                                        <li><a href="{{ url('schools/cosmetology') }}">Cosmetology</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder {{ Request::segment(1) === 'Clubs' ? 'active': '' }}">
                                    <a href="{{ url('Clubs') }}">Clubs</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('about') }}">Christian Union</a></li>
                                        {{--                                        <li><a href="{{ url('about') }}">Engeenering</a></li>--}}
                                        {{--                                        <li><a href="{{ url('about') }}">Hospitality</a></li>--}}
                                        {{--                                        <li><a href="{{ url('about') }}">Business</a></li>--}}
                                        {{--                                        <li><a href="{{ url('about') }}">Computer Science</a></li>--}}
                                        {{--                                        <li><a href="{{ url('about') }}">Cosmetology</a></li>--}}
                                        {{--                                        <li><a href="{{ url('about') }}">ICT</a></li>--}}
                                    </ul>
                                </li>

                                <li class="{{ Request::segment(1) === 'downloads' ? 'active': '' }}"><a
                                            href="{{ url('downloads') }}">Download</a></li>


                                <li class="dropdown-holder {{ Request::segment(1) === 'admissions' ? 'active': '' }}">
                                    <a href="{{ url('admissions') }}">Admissions</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('admissions') }}">Diploma Admissions</a></li>
                                        <li><a href="{{ url('admissions') }}">Certificate Admissions</a></li>
                                        <li><a href="{{ url('admissions') }}">Artisan Admissions</a></li>
                                        {{--                                        <li><a href="{{ url('admissions') }}">Professional Short Courses</a></li>--}}
                                        <li><a href="{{ url('admissions') }}">View Fees Structures</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-holder {{ Request::segment(1) === 'digitalaccess' ? 'active': '' }}">
                                    <a href="{{ url('digitalaccess') }}">Digital Access</a>
                                    <ul class="sub-menu">
                                        <li><a href="http://portal.mmttc.ac.ke">Staff/Student Portal</a></li>
                                        <li><a href="http://gmail.com">Student/Staff Emails</a></li>
                                        <li><a href="http://portal.mmttc.ac.ke">E-Library</a></li>
                                    </ul>
                                </li>

                                <li class="{{ Request::segment(1) === 'contact-mmttc' ? 'active': '' }}"><a
                                            href="{{ url('contact-mmttc') }}">Contact</a></li>
                                {{--                                <li class="{{ Request::segment(1) === 'contact-mmttc' ? 'active': '' }}">--}}
                                {{--                                    <a href="{{ url('contact-mmttc') }}">CU</a></li>--}}
                                {{--                                <li class="join-us"><a href="#" class="theme-solid-button">Join Now</a></li>--}}
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav> <!-- /.theme-main-menu -->
                </div> <!-- /.main-content-wrapper -->
            </div> <!-- /.container -->
        </div> <!-- /.header-wrapper -->
    </header> <!-- /.theme-menu-wrapper -->

    <div id="front">
        @yield('content')
    </div>
    <!--
        =============================================
            Footer
        ==============================================
        -->
    <footer class="theme-footer">
        <div class="container">
            <div class="top-footer row">
                <div class="col-sm-3 col-xs-12 footer-list">
                    <h6>Use link</h6>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-6">
                            <ul>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Service</a></li>
                                <li><a href="#">Case Studies</a></li>
                                <li><a href="#">Event</a></li>
                                <li><a href="#">Contact us</a></li>
                                <li><a href="#">Faq</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- /.footer-list -->
                <div class="col-sm-6 col-xs-12 footer-logo-widget">
                    <div class="wrapper">
                        <div class="logo"><a href="#"><img src="{{ asset('logo.JPG') }}" height="70" alt=""></a></div>
                        <p>Machakos Medical &amp; Technical Training College is a private facility approved by the
                            ministry of
                            education and offers a variety of courses.</p>
                        <ul>
                            <li><i class="flaticon-stopwatch"></i> Monday - Friday at 8am-4pm</li>
                            <li><i class="flaticon-map-line"></i> Machakos</li>
                        </ul>
                    </div>
                </div> <!-- /.footer-logo-widget -->
                <div class="col-sm-3 col-xs-12 footer-news">
                    <h6>Update News</h6>
                    <ul>
                        <li>
                            <a href="#">Student ID Cards</a>
                            <div class="date">March 31, 2019</div>
                        </li>
                        {{--                        <li>--}}
                        {{--                            <a href="#">Student ID Cards</a>--}}
                        {{--                            <div class="date">March 31, 2019</div>--}}
                        {{--                        </li>--}}
                    </ul>
                </div> <!-- /.footer-news -->
            </div> <!-- /.top-footer -->
        </div> <!-- /.container -->

        <div class="bottom-footer">
            <div class="container">
                <p>Copyright &copy; {{ date('Y') }} <a href="#" class="tran3s">MMTTC</a>
                </p>
            </div> <!-- /.container -->
        </div> <!-- /.bottom-footer -->
    </footer>


    <!-- Scroll Top Button -->
    <button class="scroll-top tran3s">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </button>


    <!-- Js File_________________________________ -->

    <!-- j Query -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('front/vendor/jquery.2.2.3.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('front/vendor/bootstrap/bootstrap.min.js') }}"></script>
    <!-- Bootstrap Select JS -->
    <script type="text/javascript"
            src="{{ asset('front/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- Camera Slider -->
    <script type='text/javascript'
            src='{{ asset('front/vendor/Camera-master/scripts/jquery.mobile.customized.min.js') }}'></script>
    <script type='text/javascript'
            src='{{ asset('front/vendor/Camera-master/scripts/jquery.easing.1.3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('front/vendor/Camera-master/scripts/camera.min.js') }}'></script>
    <!-- Mega menu  -->
    <script type="text/javascript" src="{{ asset('front/vendor/bootstrap-mega-menu/js/menu.js') }}"></script>
    <!-- WOW js -->
    <script src="{{ asset('front/vendor/WOW-master/dist/wow.min.js') }}"></script>
    <!-- owl.carousel -->
    <script src="{{ asset('front/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <!-- Fancybox -->
    <script type="text/javascript" src="{{ asset('front/vendor/fancybox/dist/jquery.fancybox.min.js') }}"></script>


    <!-- Theme js -->
    <script type="text/javascript" src="{{ asset('front/js/theme.js') }}"></script>
    {{--    <script type="text/javascript" src="{{ asset('front/js/map-script.js') }}"></script>--}}
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            // nav:true,
            responsive: {
                600: {
                    items: 3
                }
            }
        });
        $('.our-partners').owlCarousel({
            loop: true,
            margin: 10,
            // nav:true,
            responsive: {
                600: {
                    items: 3
                }
            }
        })
    </script>
</div> <!-- /.main-page-wrapper -->
</body>

<!-- Mirrored from themazine.com/html/remakri/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 Mar 2019 07:24:35 GMT -->
</html>