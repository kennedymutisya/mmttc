<div class="left-menu-inner scroll-pane">
    <ul class="left-menu-list left-menu-list-root list-unstyled">
        <li class="left-menu-list-submenu">
            <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-cog util-spin-delayed-pseudo"><!-- --></i>
                <span class="menu-top-hidden">Theme</span> Settings
            </a>
            <ul class="left-menu-list list-unstyled">
                <li>
                    <div class="left-menu-item">
                        <div class="left-menu-block">
                            <div class="left-menu-block-item">
                                <small>This menu gives possibility to construct custom blocks with any widgets,
                                    components and elements inside, like this theme settings
                                </small>
                            </div>
                            <div class="left-menu-block-item">
                                <span class="font-weight-600">Theme Style:</span>
                            </div>
                            <div class="left-menu-block-item" id="options-theme">
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options-theme" value="theme-default"
                                                   autocomplete="off" checked=""> Light
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-theme" value="theme-dark"
                                                   autocomplete="off"> Dark
                                        </label>
                                    </div>
                                </div>
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-theme" value="theme-green"
                                                   autocomplete="off"> Green
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-theme" value="theme-blue"
                                                   autocomplete="off"> Blue
                                        </label>
                                    </div>
                                </div>
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-theme" value="theme-red"
                                                   autocomplete="off"> Red
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-theme" value="theme-orange"
                                                   autocomplete="off"> Orange
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="left-menu-block-item">
                                <span class="font-weight-600">Fixed Top Menu</sup>:</span>
                            </div>
                            <div class="left-menu-block-item" id="options-menu">
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options-menu" value="menu-fixed" checked="">
                                            On
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-menu" value="menu-static"> Off
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="left-menu-block-item">
                                <span class="font-weight-600">Super Clean Mode:</span>
                            </div>
                            <div class="left-menu-block-item" id="options-mode">
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options-mode" value="mode-superclean"
                                                   checked=""> On
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-mode" value="mode-default"> Off
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="left-menu-block-item">
                                <span class="font-weight-600">Colorful Menu:</span>
                            </div>
                            <div class="left-menu-block-item" id="options-colorful">
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <div class="btn-group">
                                        <label class="btn btn-default active">
                                            <input type="radio" name="options-colorful" value="colorful-enabled"
                                                   checked=""> On
                                        </label>
                                    </div>
                                    <div class="btn-group">
                                        <label class="btn btn-default">
                                            <input type="radio" name="options-colorful" value="colorful-disabled">
                                            Off
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>

        <li class="left-menu-list-separator "><!-- --></li>
        <li class="left-menu-list">
            <a class="left-menu-link" href="/">
                <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                <span class="menu-top-hidden">Dashboard</span>
            </a>
        </li>
        <li class="left-menu-list-submenu">
            <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-gamepad"><!-- --></i>
                Global Setup
            </a>
            <ul class="left-menu-list list-unstyled">

                <li>
                    <a class="left-menu-link" href="{{ route('school.index') }}">
                        School Setup
                    </a>
                </li>
                <li>
                    <a class="left-menu-link" href="{{ route('subject.index') }}">
                        Subjects
                    </a>
                </li>
                <li>
                    <a class="left-menu-link" href="{{ route('subjectgroup.index') }}">
                        Subject Groups
                    </a>
                </li>
                <li>
                    <a class="left-menu-link" href="{{ route('subjectcombination.index') }}">
                        Subject Combination
                    </a>
                </li>
                <li>
                    <a class="left-menu-link" href="{{ route('school.index') }}">
                        Houses
                    </a>
                </li>

            </ul>
        </li>
        <li class="left-menu-list-submenu">
            <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-user"><!-- --></i>
                Students
            </a>
            <ul class="left-menu-list list-unstyled">

                <li>
                    <a class="left-menu-link" href="{{ route('students.index') }}">
                        Add Student
                    </a>
                </li>
            </ul>
        </li>
        <li class="left-menu-list-submenu">
            <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-address-book"><!-- --></i>
                Programmes
            </a>
            <ul class="left-menu-list list-unstyled">

                <li>
                    <a class="left-menu-link" href="{{ route('programme.index') }}">
                        Add Programme
                    </a>
                </li>
            </ul>
        </li>
        <li class="left-menu-list-submenu">
            <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-user-block2"><!-- --></i>
                Applications
            </a>
            <ul class="left-menu-list list-unstyled">

                <li>
                    <a class="left-menu-link" href="{{ route('application.confirm') }}">
                        View Applicants
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>