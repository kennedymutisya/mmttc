@extends('layouts.home')

@section('content')

    <div class="theme-inner-banner">
        <div class="opacity">
            <ul>
                <li><a href="">Home</a></li>
                <li>/</li>
                <li>Our Gallery</li>
            </ul>
            <h2>Our Gallery</h2>
        </div> <!-- /.opacity -->
    </div>
    <div class="our-history section-margin-top">
        <div class="container">
            <div class="row">
                 <!-- /.col- -->
            </div> <!-- /.row -->

            <!-- /.our-goal -->
            <!-- /.our-analytics -->
        </div> <!-- /.container -->
    </div>

    <div class="short-banner-two bg-two">
        <div class="opacity color-two">
            <div class="container">
                <h6>We offer quality education at friedly prices</h6>
                <a href="#" class="wow fadeInLeft animated theme-solid-button">Brochure</a>
                <a href="#" class="wow fadeInRight animated theme-line-button">Fee Structure</a>
            </div>
        </div>
    </div>
@endsection