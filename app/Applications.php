<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    public function course()
    {
        return $this->hasOne(Programmes::class,'id','course');
  }

    public function courseapp()
    {
        return $this->hasOne(Programmes::class, 'id', 'course');
    }
}
