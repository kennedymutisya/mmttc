<?php

namespace App\Http\Controllers;

use App\Programmes;
use Illuminate\Http\Request;

class ProgrammesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('programmes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Programmes $programmes)
    {

        $this->validate($request, [
            'code'       => ['required','unique:programmes,code'],
            'coursename' => ['required'],
            'gradetype'  => ['required'],
            'cert'       => ['required'],
            'closed'     => ['required'],
            'period'     => ['required','integer'],
        ]);
        $programmes->code = $request->code;
        $programmes->coursename = $request->coursename;
        $programmes->department = $request->department;
        $programmes->gradetype = $request->gradetype;
        $programmes->cert = $request->cert;
        $programmes->period = $request->period;
        $programmes->year = $request->year;
        $programmes->closed = $request->closed;
        $programmes->maxunits = $request->maxunits;
        $programmes->saveOrFail();

        return response()->json($programmes);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Programmes  $programmes
     * @return \Illuminate\Http\Response
     */
    public function show(Programmes $programmes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Programmes  $programmes
     * @return \Illuminate\Http\Response
     */
    public function edit(Programmes $programmes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Programmes  $programmes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programmes $programmes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Programmes  $programmes
     * @return \Illuminate\Http\Response
     */
    public function destroy($programmes)
    {
        $programe = Programmes::all()->find($programmes)->delete();
        return response()->json($programe);
    }
}
