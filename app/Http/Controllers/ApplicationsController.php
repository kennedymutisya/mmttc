<?php

namespace App\Http\Controllers;

use App\Applications;
use App\Mail\NewApplication;
use Illuminate\Http\Request;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function status($user)
    {
        if (\Request::hasValidSignature()) {
            $applicant = [Applications::find($user)->load('courseapp')];
            return view('application.track', compact('applicant'));

        } else {
            abort(404);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function passport(Request $request)
    {
        $applicant = Applications::all()->find($request->applicant);
        $path = $request->file('file')->store('passports');
        $applicant->passport = $path;
        $applicant->save();

        return response()->json($applicant);
    }

    public function updatekcsecert(Request $request)
    {
        $applicant = Applications::all()->find($request->applicant);
        $path = $request->file('file')->store('certs');
        $applicant->kcsecert = $path;
        $applicant->save();

        return response()->json($applicant);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Applications $apply)
    {
        $this->validate($request, [
            'course'     => ['required'],
            'fullname'   => ['required'],
            'Address'    => ['required'],
            'phone'      => ['required'],
            'email'      => ['required'],
            'nationalID' => ['required'],
            'dob'        => ['required'],
            'sex'        => ['required'],
        ]);

        $apply->course = $request->course;
        $apply->fullname = $request->fullname;
        $apply->Address = $request->Address;
        $apply->phone = $request->phone;
        $apply->email = $request->email;
        $apply->nationalID = $request->nationalID;
        $apply->dob = $request->dob;
        $apply->sex = $request->sex;
        $apply->saveOrFail();

        \Mail::to($apply->email)->send(new NewApplication($apply));
        return response()->json($apply);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function show(Applications $applications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function edit(Applications $applications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applications $applications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applications $applications)
    {
        //
    }
}
